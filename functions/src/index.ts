import * as functions from "firebase-functions";
import firebase from "firebase";
require("firebase/firestore");

const config = {
  apiKey: "AIzaSyCw0OQpWlI84GzVMbLVnbYbgS2E6fdoUpY",
  authDomain: "native-320b2.firebaseapp.com",
  projectId: "native-320b2",
  databaseURL: "https://native-320b2.firebaseio.com",
};
!firebase.apps.length && firebase.initializeApp(config);
const db = firebase.firestore();

const isValidData = (data: any) => {
  if (!data.name || !data.age || !data.color || !data.number) return false;
  return true;
};

const ebaniyESLint = functions.https;
exports.updateProfile = ebaniyESLint.onCall(async (data : any, context:any) => {
  console.log("data", data);
  console.log("data.userID", data.userID);

  if (!isValidData(data)) return false;

  await db.collection("users").doc(data.userID).set({
    name: data.name,
    color: data.color,
    age: data.age,
    number: data.number,
    id: data.userID,
  })
      .then(() => {
        console.log("Document written with userID: ", data.userID);
      })
      .catch((error) => {
        console.error("Error adding document: ", error);
      });
  return true;
});
